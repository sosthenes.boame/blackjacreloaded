package main;

import main.cards.Deck;
import main.core.Game;

public class Blackjack {
    public static void main(String[] args) {
        //Instantiate a deck object and initialize it with cards using createDeck()
        Deck deck = new Deck();
        deck.createDeck();

        //Instantiate a game object and initialize it with initializeGame()
        Game game = new Game();
        game.initializeGame();

        // Print out returned results of some methods
        System.out.println(deck.getCards());
        System.out.println(game.getPlayers());
    }
}
