package main.cards;
// Enums import
import main.cards.enums.CardValue;
import main.cards.enums.Suit;
// Java package imports
import java.util.Collections;
import java.util.Stack;

public class Deck {
    private Stack<Card> cards;

    // Creates deck of cards by creating cards
    public void createDeck() {
        cards = new Stack<>();
        for (Suit suit: Suit.values()) {
            for (CardValue value: CardValue.values()){
                Card card = new Card(suit, value);
                cards.push(card);
            }
        }
    }

    // Shuffles the cards
    public Stack<Card> shuffleCards(){
        Collections.shuffle(cards);
        return cards;
    }

    public Stack<Card> getCards() {
        return cards;
    }
}
