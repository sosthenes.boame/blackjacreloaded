package main.cards;

import main.cards.enums.CardValue;
import main.cards.enums.Suit;

public class Card {
    private Suit suit;
    private CardValue value;

    public Card(Suit suit, CardValue value) {
        this.suit = suit;
        this.value = value;
    }

    @Override
    public String toString(){
        return this.value+" of "+this.suit;
    }
}
