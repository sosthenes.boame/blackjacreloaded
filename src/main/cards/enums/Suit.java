package main.cards.enums;

public enum Suit {
    DIAMONDS, HEARTS, SPADES, CLUBS;
}
