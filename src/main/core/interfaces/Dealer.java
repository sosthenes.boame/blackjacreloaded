package main.core.interfaces;

public interface Dealer {
    void dealCards();
}
