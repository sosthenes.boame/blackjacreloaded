package main.core.interfaces;

public interface Strategy {
    void hit();
    void stick();
    void goBust();
}
