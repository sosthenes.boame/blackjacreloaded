package main.core;

import main.cards.Card;
import main.cards.Deck;
import main.core.enums.NumberOfPlayers;
import main.core.interfaces.Dealer;
import main.core.interfaces.Strategy;
import main.players.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Game implements Dealer, Strategy {
    private List<Player> players;
    private Stack<Card> shuffledDeck;

    // Methods that get called when the game starts
    public void initializeGame() {
        this.players = new ArrayList<>();
        initializePlayers(NumberOfPlayers.NUMBER_OF_PLAYERS.getNumOfPlayers());
        setShuffleDeck();
        dealCards();
    }

    // Initializes players for the game
    public void initializePlayers(int numberOfPlayers){
        for(int i=1; i <= numberOfPlayers; ++i){
            Player player = new Player("Player "+i);
            this.players.add(player);
        }
    }

    // Setter to create, and set shuffled cards to instantiated deck
    public void setShuffleDeck() {
        Deck deck = new Deck();
        deck.createDeck();
        this.shuffledDeck = deck.shuffleCards();
    }

    // Gives (deals) each player 2 cards
    @Override
    public void dealCards(){
        for (Player player: players) {
            player.setHands(shuffledDeck.pop());
            player.setHands(shuffledDeck.pop());
        }
    }

    @Override
    public void hit() {

    }

    @Override
    public void stick() {

    }

    @Override
    public void goBust() {

    }

    public List<Player> getPlayers() {
        return players;
    }
}
