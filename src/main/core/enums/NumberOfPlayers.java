package main.core.enums;

public enum NumberOfPlayers {
    NUMBER_OF_PLAYERS("Number of Players", 3);

    String str;
    int numOfPlayers;
    NumberOfPlayers(String str, int numOfPlayers) {
        this.str = str;
        this.numOfPlayers = numOfPlayers;
    }

    public int getNumOfPlayers() {
        return numOfPlayers;
    }
}
