package main.core.enums;

public enum ScoreBoundary {
    MIN_POINT("Minimum", 17),
    MAX_POINT("Maximum", 21);

    String name;
    int point;

    ScoreBoundary(String name, int point) {
        this.name = name;
        this.point = point;
    }
}
