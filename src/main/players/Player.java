package main.players;

import main.cards.Card;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private final String playerId;
    private int points;
    private List<Card> hands = new ArrayList<>();

    public Player(String playerId) {
        this.playerId = playerId;
        this.points = points;
    }

    // Adds cards to player "hands"
    public void setHands(Card hands) {
        this.hands.add(hands);
    }

    public List<Card> getHands() {
        return hands;
    }

    @Override
    public String toString(){
        return  "\n" + "Player Name: " +  this.playerId + " Points: " + this.points + " Hands: " + this.hands + "\n";
    }
}
