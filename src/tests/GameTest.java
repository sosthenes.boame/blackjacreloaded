package tests;

import main.core.Game;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class GameTest {
    @Test
    void initializePlayersTest() {
        Game game = new Game();
        game.initializeGame();
        Assertions.assertTrue(game.getPlayers().size() > 1);
    }

//    @Test
//    void dealCards() {
//    }
}