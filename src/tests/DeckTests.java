package tests;

import main.cards.Deck;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DeckTests {
    static Deck deck;

    @BeforeAll
    public static void setup(){
        deck = new Deck();
    }

    @Test
    public void deckSizeShouldEqual52Test() {
        int expected = 52;
        deck.createDeck();
        int deckSize = deck.getCards().size();
        Assertions.assertEquals(expected, deckSize);
    }
}
